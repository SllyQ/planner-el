open MuiUtils;

module List = {
  [@bs.module "material-ui/List"] external reactClass : ReasonReact.reactClass = "default";
  let make = (~style=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={"style": style |> Js.Nullable.from_opt}
    );

  module Item = {
    [@bs.module "material-ui/List"] external reactClass : ReasonReact.reactClass = "ListItem";
    let make =
        (
          ~style=?,
          ~button=?,
          ~onClick=?,
          children
        ) =>
      ReasonReact.wrapJsForReason(
        ~reactClass,
        children,
        ~props={
          "button": button |> jsOptBool |> Js.Nullable.from_opt,
          "onClick": onClick |> Js.Nullable.from_opt,
          "style": style |> Js.Nullable.from_opt
        }
      );

    module Icon = {
        [@bs.module "material-ui/List"] external reactClass : ReasonReact.reactClass = "ListItemIcon";
        let make =
            (
              ~style=?,
              children
            ) =>
          ReasonReact.wrapJsForReason(
            ~reactClass,
            children,
            ~props={
              "style": style |> Js.Nullable.from_opt
            }
          );
    };

    module Text = {
      [@bs.module "material-ui/List"] external reactClass : ReasonReact.reactClass = "ListItemText";
      let make =
          (
            ~style=?,
            ~primary=?,
            children
          ) =>
        ReasonReact.wrapJsForReason(
          ~reactClass,
          children,
          ~props={
            "style": style |> Js.Nullable.from_opt,
            "primary": primary |> Js.Nullable.from_opt
          }
        );
    };

    module SecondaryAction = {
      [@bs.module "material-ui/List"] external reactClass : ReasonReact.reactClass = "ListItemSecondaryAction";
      let make =
          (
            ~style=?,
            children
          ) =>
        ReasonReact.wrapJsForReason(
          ~reactClass,
          children,
          ~props={
            "style": style |> Js.Nullable.from_opt
          }
        );
    };
  };
};

module IconButton = {
  [@bs.module "material-ui/IconButton"] external reactClass : ReasonReact.reactClass = "default";
  let make = (~style=?, ~onClick, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={"style": style |> Js.Nullable.from_opt, "onClick": onClick}
    );
};

module Paper = {
  [@bs.module "material-ui/Paper"] external reactClass : ReasonReact.reactClass = "default";
  let make = (~style=?, ~elevation=?, ~onMouseEnter=?, ~onMouseLeave=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "style": style |> Js.Nullable.from_opt,
        "elevation": elevation |> Js.Nullable.from_opt,
        "onMouseEnter": onMouseEnter |> Js.Nullable.from_opt,
        "onMouseLeave": onMouseLeave |> Js.Nullable.from_opt
      }
    );
};

module Menu = {
  [@bs.module "material-ui/Menu"] external reactClass : ReasonReact.reactClass = "default";
  let make = (~style=?, ~anchorEl=?, ~open_, ~onRequestClose, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "style": style |> Js.Nullable.from_opt,
        "open": open_ |> jsBool,
        "onRequestClose": onRequestClose,
        "anchorEl": anchorEl |> Js.Nullable.from_opt
      }
    );

  module Item = {
    [@bs.module "material-ui/Menu"] external reactClass : ReasonReact.reactClass = "MenuItem";
    let make = (~style=?, ~onClick, children) =>
      ReasonReact.wrapJsForReason(
        ~reactClass,
        children,
        ~props={
          "style": style |> Js.Nullable.from_opt,
          "onClick": onClick
        }
      );
  }
}
