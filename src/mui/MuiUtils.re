let jsBool = (bool) => bool ? Js.true_ : Js.false_;

let jsOptBool = (optBool) =>
  switch optBool {
  | Some(b) => Some(b ? Js.true_ : Js.false_)
  | None => None
  };

let pickValue = (f, _event, value) => f(value);

let pickSelectValue = (f, _event, _key, value) => f(value);
