let makeIcon = (reactClass, children: array(ReasonReact.reactElement)) => {
  ReasonReact.wrapJsForReason(~reactClass, children, ~props=Js.Obj.empty());
};

module MoreVert = {
  [@bs.module "material-ui-icons/MoreVert"] external reactClass : ReasonReact.reactClass =
    "default";
  let make = makeIcon(reactClass)
};
