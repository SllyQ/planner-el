open Mui;

type state = {isHovered: bool};

type actions =
  | MouseEnter
  | MouseLeave;

let component = ReasonReact.reducerComponent("ListCard");

let make = (~text, _) => {
  ...component,
  initialState: () => {isHovered: false},
  reducer: (action, _state) =>
    switch action {
    | MouseEnter => ReasonReact.Update({isHovered: true})
    | MouseLeave => ReasonReact.Update({isHovered: false})
    },
  render: ({reduce, state}) =>
    <Paper
      style=(
        ReactDOMRe.Style.make(
          (),
          ~margin="12px 8px",
          ~borderRadius="4px",
          ~padding="8px",
          ~backgroundColor=state.isHovered ? "rgba(0, 0, 0, 0.08)" : "#FFF",
          ~cursor="pointer"
        )
      )
      onMouseEnter=(reduce(() => MouseEnter))
      onMouseLeave=(reduce(() => MouseLeave))>
      (ReasonReact.stringToElement(text))
    </Paper>
};

let default =
  ReasonReact.wrapReasonForJs(~component, jsProps =>
    make(~text=jsProps##text, [||])
  );
