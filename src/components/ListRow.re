open Mui;

type state = {
  moreMenuOpen: bool,
  /* TODO FIXME: this is a DOM element */
  anchorEl: int
};

type actions =
  | OpenMoreMenu(int)
  | CloseMoreMenu;

let component = ReasonReact.reducerComponent("Row");

let make = (~onClick=?, ~icon=?, ~text, ~actions=[||], _) => {
  ...component,
  initialState: () => { moreMenuOpen: false, anchorEl: 0 },
  reducer: (action, state) => switch action {
    | OpenMoreMenu(anchorEl) => ReasonReact.Update { moreMenuOpen: true, anchorEl }
    | CloseMoreMenu => ReasonReact.Update { ...state, moreMenuOpen: false }
  },
  render: ({ reduce, state }) => {
    <List.Item button=Js.Option.isSome(onClick) ?onClick>
      (switch icon {
        | None => ReasonReact.nullElement
        | Some(icon) => {
          <List.Item.Icon>
            (icon)
          </List.Item.Icon>
        }
      })
      <List.Item.Text primary=text />
      (switch actions {
        | [||] => ReasonReact.nullElement
        | actions => {
          <List.Item.SecondaryAction>
            <IconButton onClick=reduce(e => OpenMoreMenu(e##currentTarget))>
              <Icons.MoreVert />
            </IconButton>
            <Menu open_=state.moreMenuOpen onRequestClose=reduce(() => CloseMoreMenu)
              anchorEl=state.anchorEl
            >
              (actions
                |> Array.map(action => {
                  <Menu.Item key=action##label onClick=(() => {
                    reduce(() => CloseMoreMenu, ());
                    action##onClick()
                  })>
                    (ReasonReact.stringToElement(action##label))
                  </Menu.Item>
                })
                |> ReasonReact.arrayToElement
              )
            </Menu>
          </List.Item.SecondaryAction>
        }
      })
    </List.Item>
  }
};

let default = ReasonReact.wrapReasonForJs(~component, jsProps => make(
  ~text=jsProps##text,
  ~icon=?Js.Nullable.to_opt(jsProps##icon),
  ~actions=?Js.Nullable.to_opt(jsProps##actions),
  ~onClick=?Js.Nullable.to_opt(jsProps##onClick),
  [||]
));
