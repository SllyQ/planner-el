import React from "react"
import List from "material-ui/List"
import Paper from "material-ui/Paper"
import DraftsIcon from "material-ui-icons/Drafts"

import { storiesOf } from "@storybook/react"
import { action } from "@storybook/addon-actions"
import { linkTo } from "@storybook/addon-links"

import Row from "../components/ListRow.bs"
import Card from "../components/ListCard.bs"

const actions = [
  {
    label: "First option",
    onClick: action("Selected first option"),
  },
  {
    label: "Second option",
    onClick: action("Selected second option"),
  },
]

storiesOf("ListRow", module).add("Options", () => (
  <Paper style={{ width: 400, margin: 12 }} elevation={1}>
    <List>
      <Row text="A row" />
      <Row icon={<DraftsIcon />} text="With icon" />
      <Row text="With more menu" actions={actions} />
      <Row text="Clickable" onClick={action("Pressed on clickable row with icon and more menu")} />
      <Row
        icon={<DraftsIcon />}
        text="Clickable row with icon and more menu"
        actions={actions}
        onClick={action("Pressed on clickable row with icon and more menu")}
      />
    </List>
  </Paper>
))

storiesOf("ListCard", module).add("Options", () => (
  <Paper style={{ width: 400, margin: 12 }} elevation={1}>
    <List>
      <Card text="A card" />
      <Card text="Another card" />
      <Card text="Some card text" />
      <Card text="More cards!" />
      <Card text="That's a lot of cards here! This card happens to have quite a lot of text in it, so that you can see how it looks when the text doesn't fit into a single line" />
    </List>
  </Paper>
))
