import "react-chromatic/storybook-addon"
import { configure } from "@storybook/react"

function loadStories() {
  require("../src/stories")
}

configure(loadStories, module)
